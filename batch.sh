#!/bin/sh
# Scans all tracks in the directory but prints only clipping tracks
threshold=3
margin=3
clipthreshold=0.5

for file in *.flac; do
    clipvalue=$(./clipdetect $threshold $margin "$file")
    if [ $? -eq 0 ]
    then
        if [ $(echo "$clipvalue >= $clipthreshold" | bc ) -eq 1 ]
        then
            echo "$clipvalue\t$file"
        fi
    else
        echo "-1\t$file"
    fi
done
