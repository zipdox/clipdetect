#!/bin/sh
# Scans all tracks in the directory
threshold=3
margin=3

for file in *.flac; do
    clipvalue=$(./clipdetect $threshold $margin "$file")
    if [ $? -eq 0 ]
    then
        echo "$clipvalue\t$file"
    else
        echo "-1\t$file"
    fi
done
