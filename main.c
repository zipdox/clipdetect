// Clipdetect, a program for detecting clipping in music.
// Copyright (C) 2021  Zipdox

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>

void get_peaks(const char *filename, int *peak_l_dest, int *valley_l_dest, int *peak_r_dest, int *valley_r_dest, long *length_dest, int *num_channels_dest){
    AVFormatContext* container = 0;

    if (avformat_open_input(&container, filename, NULL, NULL) < 0) {
        printf("Could not open file\n");
        exit(1);
    }

    int stream_id = -1;
    for (int i = 0; i < container->nb_streams; i++) {
        if (container->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
            stream_id = i;
            break;
        }
    }

    if(stream_id < 0){
        printf("Couldn't find audio stream\n");
        exit(1);
    }

    AVStream* stream = container->streams[stream_id];

    AVCodec* codec = avcodec_find_decoder(container->streams[stream_id]->codecpar->codec_id);
    AVCodecContext *context = avcodec_alloc_context3(codec);
 
    if (avcodec_open2(context, codec, NULL) < 0) {
        printf("Could not find the needed codec\n");
        exit(1);
    }

    AVPacket *packet = av_packet_alloc();
    if(!packet){
        printf("Failed to alloc packet\n");
        exit(1);
    }

    AVFrame* frame = av_frame_alloc();
    if(!frame){
        printf("Failed to alloc frame\n");
        exit(1);
    }

    long length = 0;
    int16_t peak_l = 0;
    int16_t valley_l = 0;
    int16_t peak_r = 0;
    int16_t valley_r = 0;

    int num_channels;

    int16_t sample_l;
    int16_t sample_r;

    int response;
    while(av_read_frame(container, packet) >= 0){
        if (packet->stream_index != stream_id) continue;
        int response2 = avcodec_send_packet(context, packet);
        if (response2 < 0) {
            printf("Error while sending a packet to the decoder: %s\n", av_err2str(response));
            break;
        }

        while(response2 >= 0){
            response2 = avcodec_receive_frame(context, frame);
            if(frame->nb_samples == 0) continue;

            if(response2 == AVERROR_EOF){
                break;
            }else if(response2 < 0) {
                printf("Error while receiving a frame from the decoder: %s", av_err2str(response));
                break;
            }

            num_channels = av_get_channel_layout_nb_channels(frame->channel_layout);
            if(num_channels > 2){
                printf("Cannot handle more than 2 channels\n");
                exit(1);
            }
            for(int i = 0; i < frame->nb_samples; i++){
                length++;
                
                if(num_channels > 1){
                    sample_l = ((int16_t*) frame->data[0])[i*2];
                    if(sample_l > peak_l) peak_l = sample_l;
                    if(sample_l < valley_l) valley_l = sample_l;
                    sample_r = ((int16_t*) frame->data[0])[i*2+1];
                    if(sample_r > peak_r) peak_r = sample_r;
                    if(sample_r < valley_r) valley_r = sample_r;
                }else{
                    sample_l = ((int16_t*) frame->data[0])[i];
                    if(sample_l > peak_l) peak_l = sample_l;
                    if(sample_l < valley_l) valley_l = sample_l;
                }
                
            }
        }
    }

    avformat_close_input(&container);
    av_packet_free(&packet);
    av_frame_free(&frame);
    avcodec_free_context(&context);

    *peak_l_dest = peak_l;
    *valley_l_dest = valley_l;
    *peak_r_dest = peak_r;
    *valley_r_dest = valley_r;

    *length_dest = length;
    *num_channels_dest = num_channels;
}

void scan_clipping(const char *filename, int threshold, int peak_l, int valley_l, int peak_r, int valley_r, long *clip_count_l_dest, long *clip_count_r_dest){
    AVFormatContext* container = 0;

    if (avformat_open_input(&container, filename, NULL, NULL) < 0) {
        printf("Could not open file\n");
        exit(1);
    }

    int stream_id = -1;
    for (int i = 0; i < container->nb_streams; i++) {
        if (container->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
            stream_id = i;
            break;
        }
    }

    if(stream_id < 0){
        printf("Couldn't find audio stream\n");
        exit(1);
    }

    AVStream* stream = container->streams[stream_id];

    AVCodec* codec = avcodec_find_decoder(container->streams[stream_id]->codecpar->codec_id);
    AVCodecContext *context = avcodec_alloc_context3(codec);
 
    if (avcodec_open2(context, codec, NULL) < 0) {
        printf("Could not find the needed codec\n");
        exit(1);
    }

    AVPacket *packet = av_packet_alloc();
    if(!packet){
        printf("Failed to alloc packet\n");
        exit(1);
    }

    AVFrame* frame = av_frame_alloc();
    if(!frame){
        printf("Failed to alloc frame\n");
        exit(1);
    }

    long clip_count_l = 0;
    long clip_count_r = 0;

    int peak_samples_l = 0;
    int peak_samples_r = 0;
    int16_t prev_sample_l = 0;
    int16_t prev_sample_r = 0;
    int16_t sample_l = 0;
    int16_t sample_r = 0;

    int response;
    while(av_read_frame(container, packet) >= 0){
        if (packet->stream_index != stream_id) continue;
        int response2 = avcodec_send_packet(context, packet);
        if (response2 < 0) {
            printf("Error while sending a packet to the decoder: %s\n", av_err2str(response));
            break;
        }

        while(response2 >= 0){
            response2 = avcodec_receive_frame(context, frame);
            if(frame->nb_samples == 0) continue;
            if(frame->format != AV_SAMPLE_FMT_S16){
                printf("Track isn't s16 sample format\n");
                exit(1);
            }

            if(response2 == AVERROR_EOF){
                break;  
            }else if(response2 < 0) {
                printf("Error while receiving a frame from the decoder: %s", av_err2str(response));
                break;
            }

            int num_channels = av_get_channel_layout_nb_channels(frame->channel_layout);
            for(int i = 0; i < frame->nb_samples; i++){
                prev_sample_l = sample_l;
                prev_sample_r = sample_r;

                if(num_channels > 1){
                    sample_l = ((int16_t*) frame->data[0])[i*2];
                    if(sample_l >= peak_l || sample_l <= valley_l){
                        peak_samples_l++;
                    }else{
                        if(peak_samples_l >= threshold) clip_count_l += peak_samples_l;
                        peak_samples_l = 0;
                    }
                    sample_r = ((int16_t*) frame->data[0])[i*2+1];
                    if(sample_r >= peak_r || sample_r <= valley_r){
                        peak_samples_r++;
                    }else{
                        if(peak_samples_r >= threshold) clip_count_r += peak_samples_r;
                        peak_samples_r = 0;
                    }
                }else{
                    sample_l = ((int16_t*) frame->data[0])[i];
                    if(sample_l >= peak_l || sample_l <= valley_l){
                        peak_samples_l++;
                    }else{
                        if(peak_samples_l >= threshold) clip_count_l += peak_samples_l;
                        peak_samples_l = 0;
                    }
                }
            }
        }
    }

    avformat_close_input(&container);
    av_packet_free(&packet);
    av_frame_free(&frame);
    avcodec_free_context(&context);

    *clip_count_l_dest = clip_count_l;
    *clip_count_r_dest = clip_count_r;
}

int main(int argc, char *argv[]){
    if(argc < 4 || argc > 4){
        printf("Usage: clipdetect [threshold] [margin] [file]\n");
        exit(1);
    }

    const char *threshold_s = argv[1];
    const char *margin_s = argv[2];
    const char *filename = argv[3];
    char *endptr;
    int threshold = strtol(threshold_s, &endptr, 10);
    int margin = strtol(margin_s, &endptr, 10);

    if(threshold < 2){
        printf("The threshold needs to be at least 2\n");
        exit(1);
    }

    if(margin < 0){
        printf("Margin needs to be at least 0\n");
        exit(1);
    }

    int peak_l;
    int valley_l;
    int peak_r;
    int valley_r;
    long length;
    int num_channels;

    get_peaks(filename, &peak_l, &valley_l, &peak_r, &valley_r, &length, &num_channels);

    peak_l -= margin;
    valley_l += margin;

    long clip_count_l;
    long clip_count_r;
    scan_clipping(filename, threshold, peak_l, valley_l, peak_r, valley_r, &clip_count_l, &clip_count_r);

    double score;
    if(num_channels > 1){
        score = (clip_count_l + clip_count_r) / 2.0 / (length / 1000.0);
    }else{
        score = clip_count_l / (length / 1000.0);
    }

    printf("%f\n", score);

    return 0;
}