# clipdetect
A very crude program for detecting clipping in music. Only tested on 16-bit FLACs, might segfault on other files.

## How it works
Clipdetect operates in two passes. First it scans the tracks for the peak amplitude. That will be used as a reference for finding clipping. Then it does the actual clip detection pass. It looks for peaks in the waveform that reach the peak amplitude minus margin, and whose length reaches the threshold number of samples. The number of cliping samples is then averaged between the left and right channel if stereo, divided by number of samples in the track and multiplied by 1000. This results in the &permil; of clipping samples.

## Usage
	./clipdetect [threshold] [margin] [file]
- **threshold**: How many repeating peak samples a required to count as a clip.
- **margin**: Margin below the highest peak that should be counted as a clip.

Reasonable values for threshold and margin are 3 and 3. With these values I find that a song's clip score is generally 0&permil; if it's well-mastered. If it exceeds 0.5&permil; then there's almost definitely significant clipping. That isn't to say that clipping doesn't go undetected, this algorithm is very crude.

## Note
It's important to state that this algorithm is only reliably able to detect clipping in digitally mastered tracks, as slight variations in limiter amplitude cause the margin to not be reached.

## Building
Building on Linux is straightforward. You need to have `gcc` and `make` installed, as well as `libavcodec-dev`, `libavformat-dev` and `libavutil-dev`.
You can just run `make` and it'll compile.

For MacOSX and other Unix-like systems, the above tools can likely be installed through brew or your OS's package manager, though I don't have any experience with that.

For Windows, you're on your own.

## License
This program is licensed under the GPL-3.0. As such, commercial usage is allowed, but modified source code must be shared.